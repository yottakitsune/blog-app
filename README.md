This is my persinal blog created with <a href="https://github.com/MuYunyun/create-react-doc" target="_blank">create-react-doc</a> where to write and record some thought daily ✨✨✨.

- [Data Structure](#data-structure)
- [Design Pattern](#design-pattern)
- [FE Cloud](#fe-cloud)

### Data Structure

This section we will start tralvel in the world of data strcture together! There is no doubt the travel is so interestring that you'll enjoy it too! Before your travel of data structure, I want to tell two points it's valueble:

1. Interest and Insist.
2. Thought is more important than action.

> [Travel Of LeetCode](https://github.com/MuYunyun/blog/blob/main/LeetCode/README.md)

* [Stack](https://github.com/MuYunyun/blog/blob/main/Algorithm/data_structure/stack.md)
* [Queue](https://github.com/MuYunyun/blog/blob/main/Algorithm/data_structure/queue.md)
* [List](https://github.com/MuYunyun/blog/blob/main/Algorithm/data_structure/list.md)
* [Set](https://github.com/MuYunyun/blog/blob/main/Algorithm/data_structure/set.md)
* [Dictionary](https://github.com/MuYunyun/blog/blob/main/Algorithm/data_structure/.md)
* [Hash Table](https://github.com/MuYunyun/blog/blob/main/Algorithm/data_structure/hash_table.md)
* [Binary Tree](https://github.com/MuYunyun/blog/blob/main/Algorithm/data_structure/binary_tree.md)
* [Graph](https://github.com/MuYunyun/blog/blob/main/Algorithm/data_structure/graph.md)

<details>
  <summary>algorithm</summary>

* [Algorithm And Complexy](https://github.com/MuYunyun/blog/blob/main/Algorithm/algorithm/complexy.md)
* [Recursive](https://github.com/MuYunyun/blog/blob/main/Algorithm/algorithm/recursive.md)
* [Binary Search](https://github.com/MuYunyun/blog/blob/main/Algorithm/algorithm/binary_search.md)
* [Greedy Algorithm](https://github.com/MuYunyun/blog/blob/main/Algorithm/algorithm/greedy.md)
* [Dynamic Programming](https://github.com/MuYunyun/blog/blob/main/Algorithm/algorithm/dynamic_programming.md)

**Sort Algorithm**

* [Selection Sort](https://github.com/MuYunyun/blog/blob/main/Algorithm/algorithm/sort/selection_sort.md)
* [Quick Sort](https://github.com/MuYunyun/blog/blob/main/Algorithm/algorithm/sort/quick_sort.md)
* [Merge Sort](https://github.com/MuYunyun/blog/blob/main/Algorithm/algorithm/sort/merge_sort.md)
* [Heap Sort](https://github.com/MuYunyun/blog/blob/main/Algorithm/algorithm/sort/heap_sort.md)

算法是一种思想! 以排序算法为例, 最常见的是在数组中使用排序算法, 但是相同的思想也能用于数组对象, 甚至链表中, 比如链表中实现排序的两道题。

1. [147.Insertion Sort List](https://github.com/MuYunyun/blog/blob/main/BasicSkill/LeetCode/147.Insertion_Sort_List/README.md): insert sort in list;
2. [148.Sort List](https://github.com/MuYunyun/blog/blob/main/BasicSkill/LeetCode/148.Sort_List/README.md): merge sort in list;

</details>

### FE Cloud

This section will talk about colorful frontend world.

#### JavaScript

* [你不知道的 JavaScript](https://github.com/MuYunyun/blog/issues/2)
* [红皮书里的细节](https://github.com/MuYunyun/blog/blob/main/BasicSkill/basis/二刷高程.md)
* [探寻 JavaScript 精度问题](https://github.com/MuYunyun/blog/blob/main/BasicSkill/basis/探寻JavaScript精度问题.md)
* [函数式编程入门](https://github.com/MuYunyun/blog/blob/main/BasicSkill/编程范式/函数式编程入门.md)
* [Decorator](https://github.com/MuYunyun/blog/blob/main/BasicSkill/readES6/装饰器.md)
* [Promise](https://github.com/MuYunyun/blog/blob/main/BasicSkill/readES6/Promise.md)<a href='https://github.com/MuYunyun/repromise' target="_blank"><sub>(相关项目)</sub></a>
* [Generator](https://github.com/MuYunyun/blog/blob/main/BasicSkill/readES6/Generator.md)
* [Async](https://github.com/MuYunyun/blog/blob/main/BasicSkill/readES6/Async.md)
* [CommonJS 模块与 ES6 模块间的差异](https://github.com/MuYunyun/blog/blob/main/BasicSkill/readES6/模块.md)
* [ES6 继承与 ES5 继承的差异](https://github.com/MuYunyun/blog/blob/main/BasicSkill/readES6/继承.md)
* [扩展运算符](https://github.com/MuYunyun/blog/blob/main/BasicSkill/readES6/扩展运算符.md)
* [箭头函数](https://github.com/MuYunyun/blog/blob/main/BasicSkill/readES6/箭头函数.md)
* [Reflect](https://github.com/MuYunyun/blog/blob/main/BasicSkill/readES6/Reflect.md)

- [ ] [重构笔记](https://github.com/MuYunyun/blog/blob/main/BasicSkill/效率篇/重构改善既有代码的设计.md)

#### CSS

* [INHERITED AND NON-INHERITED](https://github.com/MuYunyun/blog/blob/main/BasicSkill/css/INHERITED_AND_NON-INHERITED.md)

<details>
  <summary>more</summary>

* [水平布局解决方案](https://github.com/MuYunyun/blog/blob/main/BasicSkill/basis/水平布局解决方案.md)
* [聊聊 BFC](https://github.com/MuYunyun/blog/blob/main/BasicSkill/css/聊聊BFC.md)
* [过渡与动画](https://github.com/MuYunyun/blog/blob/main/BasicSkill/css/css小书/过渡与动画.md)

</details>

#### TypeScript

* [TypeScript 基础篇](https://github.com/MuYunyun/blog/blob/main/TypeScript/TypeScript基础篇.md)
* [Types vs Interfaces](https://github.com/MuYunyun/blog/blob/main/TypeScript/Types_vs_Interfaces.md)
* [工具类型](https://github.com/MuYunyun/blog/blob/main/TypeScript/Utility_Types.md)

#### Node.js

* [简版 express.js 的实现](http://muyunyun.cn/blog/BasicSkill/node/implement_express.js)
* [简版 koa.js 的实现](http://muyunyun.cn/blog/#/BasicSkill/node/implement_koa.js)
* [Node.js 异步异闻录](https://github.com/MuYunyun/blog/issues/7)
<a href='https://github.com/MuYunyun/demos-of-node.js' target="_blank"><sub>(相关项目)</sub></a>
* [用 Node.js 把玩一番 Alfred Workflow](https://github.com/MuYunyun/blog/issues/6) <a href='https://github.com/MuYunyun/commonSearch' target="_blank"><sub>(相关项目)</sub></a>

#### FE CLOUD

* [SEO 在 SPA 站点中的实践](https://github.com/MuYunyun/blog/blob/main/FeCloud/seo_in_spa_site.md)
* [Talk about TC39](https://github.com/MuYunyun/blog/blob/main/FeCloud/tc39.md)
* [跨域二三事](https://github.com/MuYunyun/blog/blob/main/BasicSkill/http/cross-domain.md) <a href='https://github.com/MuYunyun/cross-domain' target="_blank"><sub>(相关项目)</sub></a>
* [HTTP 小册](https://github.com/MuYunyun/blog/blob/main/BasicSkill/http/http.md)
* [HTML5](https://github.com/MuYunyun/blog/blob/main/BasicSkill/basis/HTML5.md)
* [探寻 webpack 插件机制](https://github.com/MuYunyun/blog/blob/main/FeCloud/探寻webpack插件机制.md) <a href='https://github.com/MuYunyun/analyze-webpack-plugin' target="_blank"><sub>(相关项目)</sub></a>
* [Babel 执行机制](https://github.com/MuYunyun/blog/blob/main/FeCloud/babel执行机制.md)
* [npm 与 yarn](https://github.com/MuYunyun/blog/tree/main/FeCloud/yarn)
* [移动端场景知识](https://github.com/MuYunyun/blog/blob/main/BasicSkill/basis/移动端场景知识.md)
* [原生 JS 实现一个瀑布流插件](https://github.com/MuYunyun/FeCloud/issues/12) <a href='https://github.com/MuYunyun/waterfall' target="_blank"><sub>(相关项目)</sub></a>
* [实现一个自定义工具类库](https://github.com/MuYunyun/blog/issues/9) <a href='https://github.com/MuYunyun/diana' target="_blank"><sub>(相关项目)</sub></a>
* [走近 Python](https://github.com/MuYunyun/blog/issues/8)

#### Project Framework

* [CAS 登入流程](https://github.com/MuYunyun/blog/blob/main/BasicSkill/project_framework/CAS登入流程.md)
* [RPC 在网关项目中的实践](https://github.com/MuYunyun/blog/blob/main/BasicSkill/project_framework/RPC在点我达网关的实践一.md)
* [解读 IoC 框架 —— InversifyJS](https://github.com/MuYunyun/blog/blob/main/BasicSkill/project_framework/解读IoC框架InversifyJS.md)

> It's very thankful to point any wrong place from these articles [there](https://github.com/MuYunyun/blog/issues/new) as well. The Articles licensed under [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/deed.en) Agreement.
